﻿using Amazon.S3;
using Amazon.S3.Transfer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace S3
{
    public class UploadToS3
    {
        public async Task<string> UploadFromDisk(AmazonS3Client s3Client, string pathOnDrive, string bucketName, string s3directory, string fileName, bool isPublic = false)
        {
            var fileTransferUtility = new TransferUtility(s3Client);

            using (var fileToUpload = new FileStream(pathOnDrive, FileMode.Open, FileAccess.Read))
            {
                var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                {
                    BucketName = bucketName,
                    InputStream = fileToUpload,
                    StorageClass = S3StorageClass.StandardInfrequentAccess,
                    PartSize = 6291456, // 6 MB.
                    Key = $"{s3directory}/{fileName}",
                    CannedACL = isPublic ? S3CannedACL.PublicRead : S3CannedACL.Private
                };

                await fileTransferUtility.UploadAsync(fileTransferUtilityRequest);

                return $"https://{fileTransferUtilityRequest.BucketName}.s3.amazonaws.com/{fileTransferUtilityRequest.Key}";
            }
        }

        public async Task<string> UploadFromByteArray(AmazonS3Client s3Client, byte[] data, string bucketName, string s3directory, string fileName, bool isPublic = false)
        {
            using (MemoryStream stream = new MemoryStream(data))
            {
                using (var fileTransferUtility = new TransferUtility(s3Client))
                {
                    var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                    {
                        BucketName = bucketName,
                        InputStream = stream,
                        StorageClass = S3StorageClass.StandardInfrequentAccess,
                        PartSize = 6291456, // 6 MB.
                        Key = $"{s3directory}/{fileName}",
                        CannedACL = isPublic ? S3CannedACL.PublicRead : S3CannedACL.Private
                    };

                    await fileTransferUtility.UploadAsync(fileTransferUtilityRequest);
                    return $"https://{fileTransferUtilityRequest.BucketName}.s3.amazonaws.com/{fileTransferUtilityRequest.Key}";
                }
            }

        }


    }
}
