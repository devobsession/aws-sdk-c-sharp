﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Amazon.S3.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace S3
{
    public class DownloadFromS3
    {
        public async Task DownloadToDisk(AmazonS3Client s3Client, string pathOnDrive, string url, string fileName)
        {
            AmazonS3Uri amazonS3Uri = new AmazonS3Uri(url);

            await DownloadToDisk(s3Client, pathOnDrive, amazonS3Uri.Bucket, amazonS3Uri.Key, fileName);
        }

        public async Task DownloadToDisk(AmazonS3Client s3Client, string pathOnDrive, string bucketName, string keyName, string fileName)
        {
            GetObjectRequest request = new GetObjectRequest
            {
                BucketName = bucketName,
                Key = keyName
            };

            var filePath = Path.Combine(pathOnDrive, fileName);

            using (GetObjectResponse response = await s3Client.GetObjectAsync(request))
            using (Stream responseStream = response.ResponseStream)
            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
            {
                await responseStream.CopyToAsync(fileStream);
            }
        }


        public async Task<byte[]> DownloadToByteArray(AmazonS3Client s3Client, string bucketName, string keyName)
        {
            GetObjectRequest request = new GetObjectRequest
            {
                BucketName = bucketName,
                Key = keyName
            };

            using (GetObjectResponse response = await s3Client.GetObjectAsync(request))
            using (Stream responseStream = response.ResponseStream)
            using (var memstream = new MemoryStream())
            {
                responseStream.CopyTo(memstream);
                var bytes = memstream.ToArray();               
               return bytes;
            }
        }
    }
}
