﻿using Amazon;
using Amazon.S3;
using System;
using System.IO;
using System.Text.Json;

namespace S3
{
    class Program
    {
        static void Main(string[] args)
        {
            DownloadFromS3 downloadFromS3 = new DownloadFromS3();

            UploadToS3 uploadToS3 = new UploadToS3();

        }

        private static AmazonS3Client GetAmazonSQSClient()
        {
            var creds = GetAppConfig("D:\\aws_creds.json");
            return new AmazonS3Client(creds.AccessKey, creds.Secret, RegionEndpoint.EUWest1);
        }

        private static AwsCreds GetAppConfig(string path)
        {
            string appSettingsJson = File.ReadAllText(path);
            var config = JsonSerializer.Deserialize<AwsCreds>(appSettingsJson, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            });
            return config;
        }
    }
}
