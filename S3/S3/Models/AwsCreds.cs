﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S3
{
    public class AwsCreds
    {
        public string AccessKey { get; set; }
        public string Secret { get; set; }
        public string AccountId { get; set; }
    }
}
