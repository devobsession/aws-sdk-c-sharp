﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQS
{
    public class RedrivePolicy
    {
        public string DeadLetterTargetArn { get; set; }
        public int MaxReceiveCount { get; set; }
    }
}
