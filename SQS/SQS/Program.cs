﻿using Amazon.Runtime;
using Amazon.SQS;
using Amazon.SQS.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace SQS
{
    class Program
    {
        private static SqsMessagesApi _sqsMessages;
        private static SqsQueuesApi _sqsQueues;

        static void Main(string[] args)
        {
            var sqsClient = GetAmazonSQSClient();
            _sqsMessages = new SqsMessagesApi(sqsClient);
            _sqsQueues = new SqsQueuesApi(sqsClient);
            var queueName = "TestQueueFromSdk";
            
            var deadLetterCreateQueueResponse = _sqsQueues.CreateQueue(queueName + "_deadletter").GetAwaiter().GetResult();
            Console.WriteLine("Created: " + deadLetterCreateQueueResponse.QueueUrl);

            var queueARN = _sqsQueues.GetQueueArn(deadLetterCreateQueueResponse.QueueUrl).GetAwaiter().GetResult();
            Console.WriteLine(queueARN);

            var createQueueResponse = _sqsQueues.CreateQueue(queueName, queueARN).GetAwaiter().GetResult();
            Console.WriteLine("Created: " + createQueueResponse.QueueUrl);

            GetQueueInfo(queueName);

            SendMessagesQueue(createQueueResponse.QueueUrl);

            ReceiveMessagesQueue(createQueueResponse.QueueUrl);

            DeleteQueue(createQueueResponse.QueueUrl, queueName);
            DeleteQueue(deadLetterCreateQueueResponse.QueueUrl, queueName + "_deadletter");
        }

        private static void GetQueueInfo(string queueName)
        {
            var queueResponse = _sqsQueues.GetQueues("").GetAwaiter().GetResult();
            Console.WriteLine("Found Queues: ");
            foreach (var queueUrl in queueResponse.QueueUrls)
            {
                Console.WriteLine(queueUrl);
            }

            var queueUrlResponse = _sqsQueues.GetQueueUrl(queueName).GetAwaiter().GetResult();
            Console.WriteLine("Found QueueUrl: " + queueUrlResponse.QueueUrl);

            var attributeReponse = _sqsQueues.GetQueueAttributes(queueUrlResponse.QueueUrl).GetAwaiter().GetResult();
            foreach (var attribute in attributeReponse.Attributes)
            {
                Console.WriteLine(attribute.Key + ": " + attribute.Value);
            }

            var queueARN = _sqsQueues.GetQueueArn(queueUrlResponse.QueueUrl).GetAwaiter().GetResult();
            Console.WriteLine("Found QueueARN: " + queueARN);
        }

        private static void SendMessagesQueue(string url)
        {
            var sendMessageResponse = _sqsMessages.SendMessage(url, "Message From My App").GetAwaiter().GetResult();
            Console.WriteLine("Messages successfully sent: " + sendMessageResponse.MessageId);

            var sendMessageBatchResponse = _sqsMessages.SendMessageBatch(url, "Batch Message").GetAwaiter().GetResult();
            Console.WriteLine("Messages successfully sent:");
            foreach (var success in sendMessageBatchResponse.Successful)
            {
                Console.WriteLine("Message id: " + success.MessageId);
                Console.WriteLine("Message content MD5: " + success.MD5OfMessageBody);
            }
            Console.WriteLine("Messages failed to send:");
            foreach (var failed in sendMessageBatchResponse.Failed)
            {
                Console.WriteLine("Message id: " + failed.Id);
                Console.WriteLine("Message content: " + failed.Message);
                Console.WriteLine("Sender's fault?: " + failed.SenderFault);
            }
        }

        private static void ReceiveMessagesQueue(string url)
        {
            var receiveMessageResponse = _sqsMessages.ReceiveMessages(url).GetAwaiter().GetResult();
            foreach (var message in receiveMessageResponse.Messages)
            {
                Console.WriteLine("message: " + message.Body);

                var deleteMessageResponse = _sqsMessages.DeleteMessage(url, message.ReceiptHandle).GetAwaiter().GetResult();
                Console.WriteLine("Message deleted from queue");
            }
        }

        private static void DeleteQueue(string url, string queueName)
        {
            var purgeQueueResponse = _sqsQueues.PurgeQueue(url).GetAwaiter().GetResult();
            Console.WriteLine("Purged: " + queueName);

            var deleteQueueResponse = _sqsQueues.DeleteQueue(url).GetAwaiter().GetResult();
            Console.WriteLine("Deleted: " + queueName);
        }

        private static AmazonSQSClient GetAmazonSQSClient()
        {
            var creds = GetAppConfig("D:\\aws_creds.json");
            var awsCredentials = new BasicAWSCredentials(creds.AccessKey, creds.Secret);
            var sqsConfig = new AmazonSQSConfig()
            {
                ServiceURL = "https://sqs.us-east-2.amazonaws.com"
            };

            return new AmazonSQSClient(awsCredentials, sqsConfig);
        }

        private static AwsCreds GetAppConfig(string path)
        {
            string appSettingsJson = File.ReadAllText(path);
            var config = JsonSerializer.Deserialize<AwsCreds>(appSettingsJson, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            });            
            return config;
        }

    }


}
