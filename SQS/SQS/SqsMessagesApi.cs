﻿using Amazon.Runtime;
using Amazon.SQS;
using Amazon.SQS.Model;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace SQS
{
    public class SqsMessagesApi
    {
        private readonly AmazonSQSClient _sqsClient;

        public SqsMessagesApi(AmazonSQSClient sqsClient)
        {
            _sqsClient = sqsClient;
        }

        public async Task<SendMessageResponse> SendMessage(string queueUrl, string messageBody)
        {
            SendMessageRequest sendMessageRequest = new SendMessageRequest()
            {
                QueueUrl = queueUrl,
                MessageBody = messageBody,
            };

            SendMessageResponse response = await _sqsClient.SendMessageAsync(sendMessageRequest);
            if (response.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Http error " + (int)response.HttpStatusCode);
            }

            return response;
        }

        public async Task<ReceiveMessageResponse> ReceiveMessages(string queueUrl)
        {
            var receiveMessageRequest = new ReceiveMessageRequest()
            {
                QueueUrl = queueUrl,
                MaxNumberOfMessages = 10,
                WaitTimeSeconds = 20
            };

            ReceiveMessageResponse response = await _sqsClient.ReceiveMessageAsync(receiveMessageRequest);
            if (response.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Http error " + (int)response.HttpStatusCode);
            }

            return response;
        }

        public async Task<SendMessageBatchResponse> SendMessageBatch(string queueUrl, string messageBody)
        {
            var sendMessageBatchRequest = new SendMessageBatchRequest()
            {
                QueueUrl = queueUrl,
                Entries = new List<SendMessageBatchRequestEntry>()
                {
                    new SendMessageBatchRequestEntry("1",messageBody + "1"),
                    new SendMessageBatchRequestEntry("2",messageBody + "2"),
                    new SendMessageBatchRequestEntry("3",messageBody + "3"),
                }
            };

            SendMessageBatchResponse response = await _sqsClient.SendMessageBatchAsync(sendMessageBatchRequest);
            if (response.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Http error " + (int)response.HttpStatusCode);
            }

            return response;
        }

        public async Task<DeleteMessageResponse> DeleteMessage(string queueUrl, string receiptHandle)
        {
            var deleteMessageRequest = new DeleteMessageRequest()
            {
                QueueUrl = queueUrl,
                ReceiptHandle = receiptHandle
            };

            DeleteMessageResponse response = await _sqsClient.DeleteMessageAsync(deleteMessageRequest);
            if (response.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Http error " + (int)response.HttpStatusCode);
            }

            return response;
        }

        public async Task<DeleteMessageBatchResponse> DeleteMessageBatch(string queueUrl, List<string> receiptHandles)
        {
            List<DeleteMessageBatchRequestEntry> entries = new List<DeleteMessageBatchRequestEntry>();
            foreach (var receiptHandle in receiptHandles)
            {
                entries.Add(new DeleteMessageBatchRequestEntry()
                {
                    Id = Guid.NewGuid().ToString(),
                    ReceiptHandle = receiptHandle
                });
            }

            var request = new DeleteMessageBatchRequest()
            {
                QueueUrl = queueUrl,
                Entries = entries
            };

            DeleteMessageBatchResponse response = await _sqsClient.DeleteMessageBatchAsync(request);
            if (response.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Http error " + (int)response.HttpStatusCode);
            }

            return response;
        }
    }
}
