﻿using Amazon.Runtime;
using Amazon.SQS;
using Amazon.SQS.Model;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace SQS
{
    public class SqsQueuesApi
    {
        private readonly AmazonSQSClient _sqsClient;

        public SqsQueuesApi(AmazonSQSClient sqsClient) 
        {
            _sqsClient = sqsClient;
        }

        public async Task<CreateQueueResponse> CreateQueue(string queueName, string deadLetterQueueARN = null)
        {
            var request = new CreateQueueRequest()
            {
                QueueName = queueName,
                Attributes = new Dictionary<string, string>()
                {
                    { QueueAttributeName.VisibilityTimeout, "30"}
                },
                Tags = new Dictionary<string, string>()
                {
                    { "TagKey", "TagVal" }
                }
            };
            if (!string.IsNullOrEmpty(deadLetterQueueARN))
            {
                RedrivePolicy redrivePolicy = new RedrivePolicy()
                { 
                    MaxReceiveCount = 5,
                    DeadLetterTargetArn = deadLetterQueueARN
                };
              
                var redrivePolicyJson = JsonSerializer.Serialize(redrivePolicy, new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                });
                var x = "{ \"deadLetterTargetArn\" : \"" + deadLetterQueueARN + "\", \"maxReceiveCount\" : \"5\"}";
                request.Attributes.Add(QueueAttributeName.RedrivePolicy, redrivePolicyJson);
            }
           
            CreateQueueResponse response = await _sqsClient.CreateQueueAsync(request);
            if (response.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Http error " + (int)response.HttpStatusCode);
            }

            return response;
        }

        public async Task<SetQueueAttributesResponse> SetQueueAttributes(string queueUrl)
        {
            var request = new SetQueueAttributesRequest()
            {
                QueueUrl = queueUrl,
                Attributes = new Dictionary<string, string>()
                {
                    { QueueAttributeName.VisibilityTimeout, "60"}
                }
            };

            SetQueueAttributesResponse response = await _sqsClient.SetQueueAttributesAsync(request);
            if (response.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Http error " + (int)response.HttpStatusCode);
            }

            return response;
        }

        public async Task<TagQueueResponse> AddTags(string queueUrl)
        {
            var request = new TagQueueRequest()
            {
                QueueUrl = queueUrl,
                Tags = new Dictionary<string, string>()
                {
                    { "NewTagKey", "NewTagVal" }
                }
            };

            TagQueueResponse response = await _sqsClient.TagQueueAsync(request);
            if (response.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Http error " + (int)response.HttpStatusCode);
            }

            return response;
        }

        public async Task<UntagQueueResponse> RemoveTags(string queueUrl)
        {
            var request = new UntagQueueRequest()
            {
                QueueUrl = queueUrl,
                TagKeys = new List<string>()
                {
                    "TagKey"
                }
            };

            UntagQueueResponse response = await _sqsClient.UntagQueueAsync(request);
            if (response.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Http error " + (int)response.HttpStatusCode);
            }

            return response;
        }

        public async Task<DeleteQueueResponse> DeleteQueue(string queueUrl)
        {
            var request = new DeleteQueueRequest()
            {
                QueueUrl = queueUrl
            };

            DeleteQueueResponse response = await _sqsClient.DeleteQueueAsync(request);
            if (response.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Http error " + (int)response.HttpStatusCode);
            }

            return response;
        }
        
        public async Task<PurgeQueueResponse> PurgeQueue(string queueUrl)
        {
            var request = new PurgeQueueRequest()
            {
                QueueUrl = queueUrl
            };

            PurgeQueueResponse response = await _sqsClient.PurgeQueueAsync(request);
            if (response.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Http error " + (int)response.HttpStatusCode);
            }

            return response;
        }
        
        public async Task<ListQueuesResponse> GetQueues(string queueNamePrefix)
        {
            var request = new ListQueuesRequest()
            {
                QueueNamePrefix = queueNamePrefix
            };

            ListQueuesResponse response = await _sqsClient.ListQueuesAsync(request);
            if (response.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Http error " + (int)response.HttpStatusCode);
            }

            return response;
        }

        public async Task<GetQueueUrlResponse> GetQueueUrl(string queueName)
        {
            var request = new GetQueueUrlRequest
            {
                QueueName = queueName
            };

            GetQueueUrlResponse response = await _sqsClient.GetQueueUrlAsync(request);
            if (response.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Http error " + (int)response.HttpStatusCode);
            }

            return response;
        }

        public async Task<string> GetQueueArn(string queueUrl)
        {
            var request = new GetQueueAttributesRequest(queueUrl, new List<string> { QueueAttributeName.QueueArn });

            var response = await _sqsClient.GetQueueAttributesAsync(request);
            if (response.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Http error " + (int)response.HttpStatusCode);
            }

            return response.QueueARN;
        }

        public async Task<GetQueueAttributesResponse> GetQueueAttributes(string queueUrl)
        {
            var request = new GetQueueAttributesRequest(queueUrl, new List<string> { QueueAttributeName.All });

            GetQueueAttributesResponse response = await _sqsClient.GetQueueAttributesAsync(request);
            if (response.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Http error " + (int)response.HttpStatusCode);
            }

            return response;
        }
    }
}
